/*
 * Copyright (c) 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.config;

public class ConfigurationError extends RuntimeException {
    public ConfigurationError(String message) {
        super(message);
    }

    public ConfigurationError(Throwable t) {
        super(t);
    }

    public ConfigurationError(String message, Throwable t){
        super(message, t);
    }
}
