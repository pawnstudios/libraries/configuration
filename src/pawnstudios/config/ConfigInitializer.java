/*
 * Copyright (c) 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.config;

import org.reflections8.Reflections;
import org.reflections8.scanners.SubTypesScanner;
import org.reflections8.scanners.TypeAnnotationsScanner;
import org.reflections8.util.ClasspathHelper;
import org.reflections8.util.ConfigurationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Properties;
import java.util.Set;

public class ConfigInitializer {
    private static final Logger log = LoggerFactory.getLogger(ConfigInitializer.class);

    public static void init() {
        Reflections reflections = new Reflections(
          new ConfigurationBuilder()
            .setScanners(
              new TypeAnnotationsScanner(),
              new SubTypesScanner()
            )
            .setUrls(ClasspathHelper.forJavaClassPath())
        );
        Set<Class<?>> configClasses = reflections.getTypesAnnotatedWith(Config.class);
        try {
            for (Class<?> configClass : configClasses) {
                loadValuesFor(configClass);
            }
        } catch (IllegalAccessException | MalformedURLException e) {
            throw new ConfigurationError(e);
        }
    }

    public static String getConfigPath(String configName) {
        return "./config/" + configName + ".properties";
    }

    private static void loadValuesFor(Class<?> classToLoadValuesFor) throws IllegalAccessException, MalformedURLException {
        String configName = classToLoadValuesFor.getSimpleName().toLowerCase();
        String configPath = getConfigPath(configName);
        FileReader reader = getFileReader(configPath);
        Properties configFile = new Properties();

        try {
            configFile.load(reader);
        } catch (IOException e) {
            throw new ConfigurationError("Error reading config file " + configPath, e);
        }

        for (Field f : classToLoadValuesFor.getDeclaredFields()) {
            String propertyName = f.getName().toLowerCase();
            String propertyValue = configFile.getProperty(propertyName);
            String valueType = f.getType().getSimpleName();

            if (propertyValue == null || propertyValue.isEmpty()) {
                throw new ConfigurationError("Server configuration incomplete. Check for missing config for " + configPath + ":" + propertyName);
            }

            Object deserializedValue = deserialize(valueType, propertyValue);
            f.set(null, deserializedValue);
        }
        log.info("Loaded config from " + configPath + " into " + classToLoadValuesFor.getName());
    }

    private static FileReader getFileReader(String configPath) {
        try {
            return new FileReader(configPath);
        } catch (FileNotFoundException e) {
            throw new ConfigurationError("Missing config file " + configPath, e);
        }
    }

    private static Object deserialize(String valueType, String propertyValue) throws java.net.MalformedURLException {
        return switch (valueType.toLowerCase()) {
            case "boolean" -> Boolean.valueOf(propertyValue);
            case "int" -> Integer.valueOf(propertyValue);
            case "long" -> Long.valueOf(propertyValue);
            case "int[]" -> Arrays.stream(propertyValue.split(",")).mapToInt(str -> Integer.parseInt(str.trim())).toArray();
            case "long[]" -> Arrays.stream(propertyValue.split(",")).mapToLong(str -> Long.parseLong(str.trim())).toArray();
            case "string[]" -> propertyValue.trim().split(",");
            case "url" -> new URL(propertyValue);
            default -> propertyValue;
        };
    }
}
