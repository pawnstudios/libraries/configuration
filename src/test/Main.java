/*
 * Copyright (c) 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package test;

import pawnstudios.config.ConfigInitializer;

public class Main {
    public static void main(String[] args) {
        ConfigInitializer.init();

        System.out.println(Database.username);
        System.out.println(Database.password);
    }
}
