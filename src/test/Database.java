/*
 * Copyright (c) 2022  Alex
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package test;

import pawnstudios.config.Config;

@Config
public class Database {
    public static String username;
    public static String password;
}
